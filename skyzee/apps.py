from django.apps import AppConfig


class SkyzeeConfig(AppConfig):
    name = 'skyzee'
